import sublime
import sublime_plugin


class SuperAddLineCommand(sublime_plugin.TextCommand):
    def run(self, edit, forward):
        old_regions = self.save_regions()
        new_regions = self.new_regions(forward)

        # clear all present sel regions since they might have been displaced
        # when ensure_line_min_width was run
        self.view.sel().clear()
        # restore all sel regions by row, col locations
        self.restore_regions(old_regions + new_regions)

    def save_regions(self):
        # [(    a   )  (    b   )]
        # [(row, col), (row, col)]
        view = self.view
        return [[view.rowcol(sel.a), view.rowcol(sel.b)] for sel in view.sel()]

    def restore_regions(self, regions_locs):
        for region_bounds in regions_locs:
            a, b = [self.view.text_point(r[0], r[1]) for r in region_bounds]
            region = sublime.Region(a, b)
            self.view.sel().add(region)

    def new_regions(self, forward):
        chain_tip = [0, -1][forward]
        row_offset = [-1, 1][forward]
        new_regions = []

        sel_columns = self.selection_columns(forward)
        for col, row_chains in sel_columns.items():
            for chain in row_chains:
                row = chain[chain_tip] + row_offset
                r = self.new_region(row, col)
                new_regions.append(r)
        return new_regions

    def new_region(self, row, col):
        self.ensure_line_min_width(row, col)
        return [[row, col]] * 2

    def ensure_line_min_width(self, row, min_width):
        self.view.run_command('ensure_minimum_line_width', {'row': row, 'min_width': min_width})

    def selection_columns(self, forward):
        columns = {}

        for sel in self.view.sel():
            row, col = self.view.rowcol(sel.b)
            if not col in columns:
                columns[col] = []
            cols_chains = columns[col]

            # if either first sel column or new sel not part of last column
            if not len(cols_chains) or cols_chains[-1][-1] != row - 1:
                cols_chains.append([])

            cols_chains[-1].append(row)

        # remove chains that has nowhere to expand
        total_rows = self.view.rowcol(self.view.size())[0]
        check_against = [0, total_rows][forward]
        check_tip = [0, -1][forward]

        for col, chains in columns.items():
            columns[col] = filter(lambda list: list[check_tip] != check_against, chains)

        return columns

class EnsureMinimumLineWidth(sublime_plugin.TextCommand):
    def run(self, edit, row, min_width):
        line_region = self.view.line(self.view.text_point(row, 0))
        diff = min_width - line_region.size()

        if diff > 0:
            fill = diff * ' '
            self.view.insert(edit, line_region.end(), fill)
