# About
Improved version of Sublime Text's default Add Line command.
Allows expanding selection at the same column even on shorter lines (as if there are virtual spaces).

Requires you to use **spaces** for indentation.

Default Add Next/Previous Line keybindings are overriden.
